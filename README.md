# Userguide

The NCShare CaaS and GaaS user support website: https://userguide.ncshare.org is generated from [MarkDown](https://www.markdownguide.org/cheat-sheet/) documents hosted in this repository. The projects static pages are generated using [Material](https://squidfunk.github.io/mkdocs-material/reference/) for [MkDocs](https://www.mkdocs.org/) and updates are automatically pushed to the public [Userguide](https://userguide.ncshare.org/) Gitlab CI, following the steps defined in .gitlab-ci.yml.

## Editing the website
Edits may be completed by anyone assigned this repo as an owner or developer by commiting their changes directly in the main branch. All other users should should automatically be prompted to commit changes to another branch and submit a merge request to the main branch.  This activity will be reviewed by site administrators before pushing the changes to the public website.

Suggestions from the user community for changes and erros should be sent to [info@ncshare.org](mailto:info@ncshare.org)

### Quick edits
For quick edits to existing pages, edit files directly in Gitlab using the web ide (multiple files) or single page.  Changes will be automatically posted to the website (using Gitlab CI).

### Larger changes
For larger changes to the website you can clone the repository to your local machine and use the  (python) IDE of your choice.  Once cloned, 

Run pip install -r requirements.txt in the repository directory
Add a the document as a .md file somewhere in the /docs folder
Add the file name and path under the nav header at the bottom of /mkdocs.yml

Run mkdocs serve to view the updated website on your local machine
Commit and push to this repository, the public website will update automatically in ~5 minutes

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.


## Hints for frequent edit types

## Creating announcements
To create an announcement, add the following (sample) code to the bottom of theme/main.html:

{% block announce %}
    {{ super() }}
    <img style='vertical-align:middle;' src='/rcsupportdocs/img/Infobox_info_icon.svg' width="20">
    <div style='vertical-align:middle; display:inline;'>
        sample annoucement with URL template <a href="https://rc.duke.edu/dcc-changes/">https://rc.duke.edu/dcc-changes/</a>
    </div>
{% endblock %}


## Project layout

    mkdocs.yml    # The configuration file, inlclude a nav heading that specifies the site menu
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
