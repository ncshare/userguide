---
hide:
  - navigation
  - toc
---

# Powerful computational tools to accelerate stem higher education and research in North Carolina.

<div class="callout" markdown>
[NCShare Compute as a Service (CaaS) and GPU as a Service (GaaS)](https://ncshare.org) is a program established and funded by the U.S. National Science Foundation to help researchers and educators to utilize computing systems and services – at no cost to you. NCShare accommodates a wide range of computational needs, from interactive sessions using tools like MATLAB, RStudio, and Jupyter Notebooks, to parallel workflows for large-scale computations.
</div>

Self-service computing allocations are provided for faculty and students of [NCShare Participants](about/participants.md). If analysis needs exceed those allocations, there are flexible options to increase computational and storage resources by request.

New to NCShare? You need to:

[Sign-up for an account here.](guides/accountreg.md){:target="_blank"  .md.button  .md-button--primary}

<div class="grid cards" markdown>

-  <span class="accent-text larger-text">Basic Computing Allocations for Courses</span>

    ---
   [Overview of resources](services/student.md)
  
    * [Get Started with RStudio](guides/rstudio/index.md)



    [:octicons-arrow-right-24: Access Container Manager](https://cmgr.ncshare.org)

-  <span class="accent-text larger-text">Advanced Computing Allocations for Research</span>

    ---
   [Overview of resources](services/advanced.md)
  
    * [Get Started with OnDemand](guides/ondemand/index.md)



    [:octicons-arrow-right-24: Access NCShare OnDemand](https://ood.ncshare.org)

-  <span class="accent-text larger-text">NCShare Office Hours</span>

    ---
    Questions about how to use NCShare? What research support options are available? Sign up for our virtual office hours:
   
    * [March 4, 10 AM](https://duke.zoom.us/meeting/register/KwZyTg6VTgWOdhs4mIDS5w)
    * [March 7, 10 AM](https://duke.zoom.us/meeting/register/mB2S2AxDRe-38KjYwAL8tw)
    * [March 14, 11 AM](https://duke.zoom.us/meeting/register/uMpCakJUS4ugVOIXbGZsKg)
    * [March 21, 10 AM](https://duke.zoom.us/meeting/register/ktYXEJ9ZQROdmnSgHz7OWg)
    * [March 25, 11 AM](https://duke.zoom.us/meeting/register/-pwH4RPsSXK6OlmbzOlkRQ)
    * [March 28, 11 AM](https://duke.zoom.us/meeting/register/7dKO4hRBR1S-xWQiwq3RUA)

    In this comprehensive workshop, we will introduce you to the basics of the Python programming language using the NCShare course environment.
  
    * [March 19, 9-11 AM](https://duke.zoom.us/meeting/register/6ntLFgg6RAKZ2Ie7IIGiPg) 


    [:octicons-arrow-right-24: Frequently Asked Questions](about/faq.md)


</div>
