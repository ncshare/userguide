# Frequently Asked Questions

??? question "When should I used standard computing resources versus advanced computing resources?"
    
    Computing resources provided through container manager are designed to support academic courses. They are light weight self-service resources that are easy to access through a web browser. Each container is backed by 2 cores and 4 GB and is intended for active coding and running basic tasks like labs or assignments.

    These resources are likely not sufficient for large computing tasks associated with research, big data, or specialized research software. For those, we recommend advanced computing resources for research.



??? question "Where can I get help?"
    
    Users from participating instititutions can contact their [support team](participants.md) directly.

    If you are IT Staff at a participating institution and need assitance, email info@ncshare.org.

??? question "How do I sign up to be a NCShare point of contact and support allocations at my college / university?"

    Email [info@ncshare.org](mailto: info@ncshare.org)
