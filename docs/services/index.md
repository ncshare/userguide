# Services

After [signing-up](../guides/accountreg.md) for NCShare, browse the available services below to learn more or launch the service and start computing!


## [General Computing Resources](student.md)

[NCShare Container Manager](https://cmgr.ncshare.org)
The Container Manager service provides the NCShare community self-service access to specialized software packages without installing complex software on your own computer. Container reservations last for a semester and provide access via web browser to packages such as RStudio and JupyterLab hosted in MCNC's datacenter.

## [Advanced Computing Resources](advanced.md)
NCShare provides full cluster computing resources in support of research in the region.

In addition to direct access for researchers, NCShare supports usage through [OSG and OSDF](https://osg-htc.org/services/access-point)

## [Data Hosting for Research](advanced.md)

More info coming soon.



