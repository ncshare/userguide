# Computing Resources

The [Container Manager](https://cmgr.ncshare.org) service provides the NCShare community easy access to specialized software packages without installing complex software on your own computer.

Container reservations last for a semester and provide access via web browser to packages such as RStudio and JupyterLab hosted in MCNC's datacenter. 

A container is a pre-built software environment that provides web browser access to software such as Jupyter or RStudio. The container is private to you and allows you to use specialized computational software without having to install it yourself. A number of courses use custom software containers for coursework so that all students are using the same software environment. 

Access to these services is self-service through [Container Manager](https://cmgr.ncshare.org) for all users at [NCShare participating organizations](../about/participants.md).


## For Instructors

Instructors can request custom Linux Docker containers with pre-loaded software packages (like Rstudio and Jupyter Notebooks) for courses and workshops. Once built, students can claim and access resources through our Virtual Container Manager service.  With containers, faculty and their designees have the opportunity to update the container’s software packages throughout the semester using the [GitLab](https://gitlab.com) project set up when the container automation was created. In addition to private storage space for each student, course storage space is provided for the instructor to share large datasets; the course storage space is made available as a read-only volume for the student containers.

To request a new container or use virtual machines in your course, complete and submit a request through your school [point of contact](../about/participants.md) at least 3 weeks prior to date needed (potentially more time will be needed at the start of the semester).

Be prepared to provide the following information:
* Give a display name and additional details (date needed, number of vms/containers required
* Jupyter Notebook or RStdio?
* Any details about any custom needs for this container
* A test script (the most resource-intensive assignment is a good candidate)
