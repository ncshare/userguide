
# Sign-up for an account to access to NCShare resources

The preferred method to gain access to NCShare is for users at [participating institutions](../about/participants.md) to sign-up for services using their home institution account through [COmanage Registry](https://incommon.org/software/comanage/) which is provided by [InCommon/Internet2](https://incommon.org/about/).

This process allows you to use your login and password from your home institution to access resources provided by NCShare. New users will have to go through a 1 time process. 

Existing NCShare COmanage users can manage their settings, including SSH keys [here](https://ncshare-com-01.ncshare.org/registry)

## Sign-up for an account with NCShare through COmanage

1.) Start your sign-up with [NCShare COmanage](https://ncshare-com-01.ncshare.org/registry/co_petitions/start/coef:1){:target="_blank"  .md.button  .md-button--primary}

2.) Find and select your institution

<p align="center">
    <img src="../../images/comanage/comanageselectinstitution.png" style = "border: 2px solid grey;" width="600">
</p>

3.) You will now be redirected to your home institution to login with your normal username and password

4.) After succesful login, you will be returned to the NCShare enrollment page and must verify your name and email address and then select `SUBMIT` at the bottom of the page

<p align="center">
    <img src="../../images/comanage/enrollmentform.png" style = "border: 2px solid grey;" width="600">
</p>

5.) After successfully setting up your account, you will be directed to our NCShare documentation to browse and launch available services.




