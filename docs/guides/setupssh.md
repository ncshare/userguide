# Setup SSH Keys for NCShare Cluster Computing

Users who would like to directly interact with the NCShare Cluster Computing environment through the [Slurm Workload Manager](https://slurm.schedmd.com) may do so by enabling SSH key authentication from their workstation.

Users who are not comfortable using the command line can access cluster resources through Open OnDemand, which provides a access to interactive sessions hrough your browser. The [Open OnDemand service.](../services/advanced.md#ncshare-ondemand) provides acesss to the DCC through RStudio, Jupyter, or Linux desktop sessions.

!!! info "Warning"
    
    If you have not already signed up for NCShare resources, first [sign-up for an account](accountreg.md).

## SSH Keys
Setting up ssh keys from your workstation will enables login via ssh and will simplify file transfer process by creating a secure key-based session between your workstation and the NCShare Cluster.

**Step 1.** To generate a key pair run the following (on your local workstation) from your command shell prompt:

`ssh-keygen -t ed25519`

You should get prompted for what file to save the key in. The default should be fine (just hit enter).

Next you should be prompted for a passphrase. Pick something secure that you can remember.

The public key generated will be stored in the file:

`~/.ssh/id_ed25519.pub`

**Step 2.** View and copy your ssh key:

`cat ~/.ssh/id_ed25519.pub`

Please be sure that you do not accidentally leave off the .pub. The file ~/.ssh/id_ed25519 (without the .pub) is your private key file, which you should NEVER share with ANYONE.

Note that the contents of this file (what is displayed by cat) should look something like this:

ssh-ed25519 AAUAC3NzaC1lZDI1NTE5AAAAIDhkOqqB+kRDIDp86EGo0hNvogOxwJmM5kxx/aYCzOrG kk338@CDSS-CPP0H34NX2

**Step 3.** Add your public key to your NCShare profile by copying to your “SSH Public key” under your user profile (top right) “Authenticators” at: [NCShare COmanage](https://ncshare-com-01.ncshare.org/registry).

Find the section titled "Authenticators." In that section there should be clickable action that says "Manage". Click that to expand this section, and you should be able to upload your public key. 

!!! info "Can't find your public key?"

    You will have to browse your workstation to upload your public key.  Since the .ssh directory is a hidden directory you will either need the exact path or show hidden files to find your public key. Make sure you upload the file that ends in `.pub`.

While in COmanage, make note of your COmanage assigned `UID` for use in your next step.

**Step 4.** Test your connection. Note `Enter passphrase for key` should display instead of `Password` 

    kk338@CDSS-JNAMB7UNXW ~ % ssh kkilroy1@login.ncshare.org
    The authenticity of host 'login.ncshare.org (198.86.179.8)' can't be established.
    ED25519 key fingerprint is SHA256:OC5NhEm80c0UVlTOYw0KFGqCwwDNGW1FqSjUasjHDE4.
    This key is not known by any other names.
    Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
    Warning: Permanently added 'login.ncshare.org' (ED25519) to the list of known hosts.
    Enter passphrase for key '/Users/kk338/.ssh/id_ed25519': 
     
    Welcome to Ubuntu 22.04.5 LTS (GNU/Linux 5.15.0-125-generic x86_64)

        * Documentation:  https://help.ubuntu.com
        * Management:     https://landscape.canonical.com
        * Support:        https://ubuntu.com/pro

    kkilroy1@login-01:~$ 


## Additional Tips for Advanced Users

### SSH Key Management

An "authentication agent" for SSH keys is typically referred to as "ssh-agent" which is a built-in tool on most operating systems that stores your SSH private keys in memory, allowing you to authenticate without re-entering your passphrase every time you connect to a server; while a "password manager" like 1Password helps with management of passwords as well as SSH keys.  **It will likely makes sense for you use either a password manager or ssh-agent, but not both**: 

**1Password (password manager)**

* To get started:[1Password](https://oit.duke.edu/service/1password/)
* Key Management: [1Password Manage Keys](https://developer.1password.com/docs/ssh/manage-keys/) 

**ssh-agent (Linux)**

Most modern Linux desktop environments will automatically prompt for and manage your ssh private key passphrase, if not,run the following: 
`ssh-add ~/.ssh/id_ed25519`  

**ssh-agent (Mac)**

macOS 12.0 or newer, run:

    ssh-add --apple-use-keychain ~/.ssh/id_ed25519

macOS older than 12.0, run:

    ssh-add -K ~/.ssh/id_ed25519

(and then type the password you just used) which will add add the key to the authentication agent, and also save the password in your keychain.

Then edit `~/.ssh/config` and add these lines to it: 

    Host *
    UseKeychain yes
    AddKeysToAgent yes
    IdentityFile ~/.ssh/id_ed25519
    

**ssh-agent (Windows)**: 

* Documentation [Windows ssh-agent](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_keymanagement) 

Running the following works for Windows, as well: `ssh-add ~/.ssh/id_ed25519`  

View more general information about ssh public key authentication at [ssh.com](https://www.ssh.com/ssh/public-key-authentication#key-pair---public-and-private).