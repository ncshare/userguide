# Our Participating Organizations

NCShare is committed to advancing scientific computing and inovate STEM education at North Carolina's historically marginalized institutions. Our NCShare Compute as a Service (CaaS) staff assist participating community members with obtaining computing and storage resources to carry out their research and education efforts. Inquire at [info@ncshare.org](mailto:info@ncshare.org) to become a participating institution.

## Current participants:

Participant | Participant Support | Legacy (Container Manager) | Current Environment
------------|---------------------|-------------------|-------------------
Campbell University || yes | yes
Catawba College | | yes | yes
Chowan University | | yes | yes
Davidson College | Brian Little brlittle@davidson.edu | yes | *_needs testing_
Duke University | [rc.duke.edu](https://oit.duke.edu/service/research-computing/) oitresearchsupport@duke.edu | yes | yes
East Carolina University | | yes | *_needs testing_
Elon University | | yes | *_needs testing_
Fayetteville State University | | yes | yes 
Guilford College | | yes | *_in progress_
Meredith College || yes | yes
North Carolina Agricultural & Technical State University | | yes | yes
North Carolina Central University | | yes | no
North Carolina School of Science and Math | | no | yes
North Carolina State University | | yes | yes
University of North Carolina, Chapel Hill | | yes | yes
University of North Carolina, Charlotte | | yes | yes
University of North Carolina, Pembroke | | no | yes
University of North Carolina, Wilmington || yes | no
Wake Forest University | |  | yes
Winston Salem State University | | yes | *_needs testing_
