# NCShare Support

NCShare is delivered as a service as a consortium that includes: [MCNC](https://www.mcnc.org), [Davidson College](https://www.davidson.edu/offices-and-services/technology-innovation/services/it-professional-services), [Duke University](https://oit.duke.edu/service/research-computing/), and [North
Carolina Central University (NCCU)](https://www.nccu.edu/administration/its). With the newest NCShare award, we additionally welcome [North Carolina Agricultural and Technology State University (A&T)](https://hub.ncat.edu/administration/its/index.php) and [University of North Carolina – Chapel Hill (UNC-CH)](https://its.unc.edu) to the team.

This consortium helps to serve advanced educational and research computing resources for multiple [participating Colleges and Universities](https://userguide.ncshare.org/about/participants/) in the state. 

All individuals with IT support responsibilies for educational and research computing at our consortium partners and participating institutions are invited to to partipate in the support of NCShare services.  Interested parties should email [info@ncshare.org](mailto:info@ncshare.org) to be added to our mailing list and [support repository](https://gitlab.com/ncshare/support).