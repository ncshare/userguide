Visit our [FAQ](/faq.md) for common problems and issues.

For all other questions and issues, NCShare users should contact their [local support team](participants.md).

\