# R Studio Setup

This guide will introduce one of the many ways to use RStudio at NCShare. It will demonstrate:

* How to login and access RStudio through container manager

    ??? info "R versus RStudio"
        R is the name of the programming language itself and RStudio is a convenient interface.

* How to move files from your computer to and from RStudio
* How to use [GitHub](https://github.com) for version control
    
    ??? tip "The Importance of Git"
        Git is a version control system (like "Track Changes" features from Microsoft Word but more powerful) and GitHub is the home for your Git-based projects on the internet (like DropBox but much better). This guide assumes you will be using GitHub, which has free accounts for educators, but if you are an experienced git user, use any service you would like. More information on how to use GitHub can be found [here](https://docs.github.com/en/get-started/start-your-journey/hello-world).

* Read in your data and create a simple visualization using `ggplot2`
* Gain practice in writing a reprodible report using Quatro

## Log in to RStudio

- Go to <https://cmgr.ncshare.org> and login with your home institutional login

??? warning "First time logging in?"

    If you havent logged into NCShare resources yet, you will need to [login and register your institutional login](../accountreg.md) with NCShare.

- Select `RStudio` (note if you have not already done so, you will need to reserve a container), login, and start your session

<p align="center">
    <img src="../../images/containermanager/containercontrols.png" style = "border: 2px solid grey;" width="600">
</p>


??? tip "Additional Container Controls"

    Next to the `Login` button, you will see a `Request Restart` button. This option is helpful if your RStudio freezes when you try and run a bad command within your session. 

You should now see a standard RStudio interface. Below are the components of the RStudio IDE.

<p align="center">
    <img src="../../images/rstudio/RStudioIDE.png" style = "border: 2px solid grey;" width="800">
</p>


## Set up your SSH key

You will authenticate GitHub using SSH.

???+ note 

    Your Git settings are saved within your NCShare container, so this process only needs to be completed 1 time per container reservation.

-   Type `credentials::ssh_setup_github()` into your RStudio ***console***.
-   R will ask "No SSH key found. Generate one now?" You should click 1 for yes.
-   You will generate a key. It will begin with "ssh-rsa...." R will then ask "Would you like to open a browser now?" You should click 1 for yes.
-   You may be asked to provide your GitHub username and password to log into GitHub. After entering this information, you should paste the key in and give it a name. You might name it in a way that indicates where the key will be used, e.g., `NCShare RStudio`).

## Configure Git

There is one more thing we need to do before getting started on our project. Specifically, we need to configure your git so that RStudio can communicate with GitHub. This requires two pieces of information: your name and email address.

To do so, you will use the `use_git_config()` function from the `usethis` package.

Type the following lines of code in the **console** in RStudio filling in your name and the email address associated with your GitHub account.

For example, mine would be

```{r}
usethis::use_git_config(
  user.name = "kakilroy", 
  user.email = "kk338@duke.edu"
  )
```

You are now ready interact with GitHub via RStudio!
